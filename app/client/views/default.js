'use strict';

/**
 * @class Represents MyWidget UU widget.
 */
function uuWidget() {
    var myWidget = new UUWidget({
        loginToken: "/Sites/Tasks/BaseWidget/my_widget/app/server/access", // Comment for Server!!!
        success: function (env) {
            var widget = new MyWidget(env);
        },
        error: function (env) {
            $('<div></div>').html('Widget "MyWidget" is not ready for use in view "default"!').appendTo('body');
        }
    });
}

var MyWidget = UUClass({
    authCfg: {
        'Authorization': "Basic OEozV1RtTEY6NmxKc2xVNlI5RA=="
    },
    id: 'widgetTable',
    
    /**
     * @constructor
     * @param {json} env
     * env.urlVariable.data={"sr":"UU:URI:TC.."}
     * env.urlVariable.options={'pageSize':'30','mode':'standard|debug','tabHeight':'500'}
     * set 'mode':'debug' enable conditional alert, console.log!
     */
    // setting the stage - setting up loader and fetching data for the dataTable
    constructor: function (env) {
        var widget = this;
        widget.viewModel = {
            headers: [],
            rows: []
        };

        //region Environment
        // Set Default Widget urlVariables
        env = env || {};
        env.urlVariables = env.urlVariables || {};
        env.urlVariables.data = env.urlVariables.data || {};
        env.urlVariables.data.sr = env.urlVariables.data.sr || {};
        env.urlVariables.options = env.urlVariables.options || {
                "mode": "debug"
            };
        env.urlVariables.options.mode = env.urlVariables.options.mode || 'standard';  //'debug'
        env.urlVariables.referreruri = env.urlVariables.referreruri || '';
        //endregion

        // TODO ask for purpose
        //widget.id = "#My-Widget";
        widget.loginToken = env.loginToken;
        widget.debug = (env.urlVariables.options.mode === 'debug');

        widget.loader = $('<div></div>').attr('id', 'loader').appendTo('body');
        widget.loader.jqxLoader({
            width: 100,
            height: 60,
            imagePosition: 'top',
            autoOpen: true,
            theme: 'uuTheme',
            text: widget.localizationConfig.loadingText
        });

        var headerRequest = $.ajax({
            type: 'GET',
            url: 'https://cmd.plus4u.net/ucl-educua/Course/getScoreColumnList/exec?uuUri=ues%3ATGA-BT%5B84723967990090703%5D%3A34339947381958586%5B34339947381958586%5D&courseVersionCode=BIO-3.2',
            cache: true,
            contentType: false,
            headers: widget.authCfg
        });

        var dataRequest = $.ajax({
            type: 'GET',
            url: 'https://cmd.plus4u.net/ucl-educua/Course/getScoreDataList/exec?uuUri='
                //+ 'ues%3ATGA-BT%5B84723967990090703%5D%3A34339947381958586%5B34339947381958586%5D',
            + encodeURIComponent('ues:TGA-BT[84723967990090703]:34339947381954828[34339947381954828]'),
            cache: false,
            contentType: false,
            headers: widget.authCfg
        });

        try {
            $.when(headerRequest, dataRequest)
                .done(function (headerResponse, dataResponse) {
                    widget.viewModel.headers = headerResponse[0].data;
                    widget.viewModel.rows = dataResponse[0].data;
                    if (!(widget.viewModel.headers && widget.viewModel.headers.length)) {
                        widget.showError(widget.localizationConfig.dataError);
                    } else {
                        widget.createDataTable(widget.viewModel);
                    }
                })
                .fail(function (jqXHR, textStatus, errorThrown) {
                    (widget.debug) && (console.log(JSON.stringify(jqXHR) + "\n" + textStatus + "\n" + errorThrown));
                    widget.showError(widget.localizationConfig.connectionError);
                })
                .always(function () {
                    widget.loader.jqxLoader("close");
                })
        } catch (error) {
            widget.showError('Programátor to zkazil :(');
        }

    },

    // jqxDataTable and it's config
    createDataTable: function (viewModel) {
        var widget = this;

        // datatable itself
        $('<div></div>').attr('id', widget.id)
            .appendTo('body')
            .jqxDataTable({
                aggregatesHeight: 30,
                altRows: true,
                columns: widget.prepHeadersFormat(viewModel.headers),
                columnsResize: true,
                editable: true,
                editSettings: {
                    saveOnPageChange: true,
                    saveOnSelectionChange: true,
                    cancelOnEsc: true,
                    saveOnEnter: true,
                    editSingleCell: true,
                    editOnDoubleClick: true
                },
                enableHover: true,
                filterable: true,
                filterMode: 'simple',
                localization: widget.localizationConfig,
                ready: function () {
                    widget.styleTweak();
                    widget.initStudyTypeFilter();
                },
                sortable: true,
                source: widget.prepData(viewModel),
                showAggregates: true,
                theme: 'uuTheme'
            });

        // single click edit
        // jqxDataTable is catching all clicks, we have to use it's handler
        $('#' + widget.id).on('rowClick', function (event) {
            // we do get row index in event.args.index
            // BUT it got bugged with filter modifications (stop working for empty filter)
            // so we hack correct index out of event with jQuery
            var parseRowId = new RegExp('row([0-9]+)' + widget.id);
            var rowIndex = parseInt($(event.args.originalEvent.target).parent().attr('id').replace(parseRowId, '$1'));
            $('#' + widget.id).jqxDataTable('beginCellEdit', rowIndex, event.args.dataField);
        });
    },

    // studying | finished | all filter
    initStudyTypeFilter: function () {
        var widget = this;

        // button init
        $('#filter' + widget.id).append('<div class="jqx-input-group-addon jqx-fill-state-normal-uuTheme stateTypeFilterBtn">' +
            widget.localizationConfig.all + '</div>');
        $('.stateTypeFilterBtn').click(toggleStateTypeFilter);

        // default filter - 'studying'
        toggleStateTypeFilter();

         function toggleStateTypeFilter(event) {
            if (widget.stateTypeFilterSetting.final) {
                // removing all filters
                $('#' + widget.id).jqxDataTable('removeFilter', 'stateType');
                $('.stateTypeFilterBtn')
                    .text(widget.localizationConfig.noSTFilter)
                    .removeClass('finalFilterBtn')
                    .addClass('noFilterBtn');
                widget.stateTypeFilterSetting.final = false;
            } else {
                var filtervalue = '';
                if (widget.stateTypeFilterSetting.active) {
                    // removing studying, adding final
                    $('#' + widget.id).jqxDataTable('removeFilter', 'stateType');
                    filtervalue = 'FINAL';
                    $('.stateTypeFilterBtn')
                        .text(widget.localizationConfig.finalSTFilter)
                        .addClass('finalFilterBtn');
                    widget.stateTypeFilterSetting.final = true;
                    widget.stateTypeFilterSetting.active = false;
                } else {
                    // no filters -> adding active
                    filtervalue = 'ACTIVE';
                    $('.stateTypeFilterBtn')
                        .text(widget.localizationConfig.studyingSTFilter)
                        .removeClass('noFilterBtn');
                    widget.stateTypeFilterSetting.active = true;
                }

                // creating filter for jqxDataTable API - see it's documentation
                var filtergroup = new $.jqx.filter();
                var filter = filtergroup.createfilter('stringfilter', filtervalue, 'equal');
                filtergroup.addfilter(0, filter);
                $('#' + widget.id).jqxDataTable('addFilter', 'stateType', filtergroup);
            }
            $('#' + widget.id).jqxDataTable('applyFilters');
        }

        // we want to keep stateType filter when simple filter is cleared
        $('#' + widget.id).on('filter',
            function (event) {
                if (widget.filterPassFlag)
                    return;
                if (widget.stateTypeFilterSetting.active || widget.stateTypeFilterSetting.final) {
                    // empty filter object, restoring stateType filter setting
                    var filtervalue = '';
                    if (widget.stateTypeFilterSetting.active) {
                        filtervalue = 'ACTIVE';
                    } else {
                        filtervalue = 'FINAL';
                    }

                    // creating filter for jqxDataTable API - see it's documentation
                    var filtergroup = new $.jqx.filter();
                    var filter = filtergroup.createfilter('stringfilter', filtervalue, 'equal');
                    filtergroup.addfilter(0, filter);
                    $('#' + widget.id).jqxDataTable('addFilter', 'stateType', filtergroup);
                    widget.filterPassFlag = true;
                    $('#' + widget.id).jqxDataTable('applyFilters');
                    widget.filterPassFlag = false;
                    console.log('fire');
                }
            });
    },

    localizationConfig: {
        // jqxDataTable strings
        decimalseparator: ',',
        thousandsSeparator: ' ',
        pagergotopagestring: 'Jít na stránku',
        pagershowrowsstring: 'zobraz řádky',
        pagerrangestring: ' z ',
        pagerpreviousbuttonstring: 'předchozí',
        pagernextbuttonstring: 'další',
        pagerfirstbuttonstring: 'první',
        pagerlastbuttonstring: 'poslední',
        filterclearstring: 'Zrušit hledání',
        filtersearchstring: 'Hledat:',
        emptydatastring: 'Žádná data k zobrazení',

        // my custom strings
        averageLabel: 'Průměr: ',
        connectionError: 'Chyba v připojení',
        dataError: 'Chyba v datech',
        loadingText: 'Načítání...',
        sumHeaderText: 'Součet',
        valueSaved: 'Uloženo',

        // stateType filters
        studyingSTFilter: 'Studující',
        finalSTFilter: 'Absolventi',
        noSTFilter: 'Všichni'
    },

    // formatting ugly data
    prepHeadersFormat: function (headersArray) {
        var widget = this;

        for (var i = 0; i < headersArray.length; i++) {
            var header = headersArray[i];
            if (i === 0) {
                // first column is very long guid
                header.width = 325;
            } else if (i === 1) {
                // second column is name - potentially long
                header.width = 175;
            } else {
                // rest is formatted for the same length (set by the longest header - "Seminární práce")
                header.width = 110;
            }
            // 7th and further columns have numbers in cells
            if (i > 6) {
                header.cellsAlign = 'right';
                // input validation (see jqxDataTable API documentation)
                header.cellsFormat = 'f1';
                header.aggregates = ['avg'];
                header.aggregatesRenderer = function (aggregates, column, element) {
                    var renderString = "<div class='aggregateCell'>"
                    renderString += aggregates.avg || "-";
                    renderString += "</div>";
                    return renderString;
                };

                // sum is in last column
                if ((i + 1) !== headersArray.length)
                    header.cellClassName = 'editableUUCell'
                else
                    header.cellClassName = 'sumUUCell';
            }
        }

        // fixing wrong data on UU server
        headersArray[2].dataField = "studentUuid";
        headersArray[5].aggregatesRenderer = function (aggregates, column, element) {
            return "<div class='aggregateLabel'>" + widget.localizationConfig.averageLabel + "</div>";
        };
        headersArray[5].aggregates = ['sum'];
        headersArray[11].text = widget.localizationConfig.sumHeaderText;

        console.log(headersArray);
        return headersArray;
    },

    // adapting data for easy use in jqxDataTable
    prepData: function (viewModel) {
        var widget = this;

        var dataFields = [];
        for (var i = 0; i < viewModel.headers.length; i++) {
            dataFields.push({
                name: viewModel.headers[i].dataField,
                type: (i > 6) ? "int" : "string"
            })
        }

        return new $.jqx.dataAdapter({
            localData: viewModel.rows,
            dataType: 'json',
            dataFields: dataFields,
            updateRow: function(rowID, rowData, commit) {
                widget.updateRowHandler(rowID, rowData, commit, widget);
            }
        });
    },

    // for second, parallel filter
    stateTypeFilterSetting: {
        active: false,
        final: false
    },

    // displays error message with proper styles
    // deprecated - use showNotification
    showError: function (msg) {
        var widget = this;
        (widget.debug) && console.log(msg);
        $('<div></div>').html(msg).addClass('uuDataTableErrorMsg').appendTo('body');
    },

    // for updating user with info about POST requests
    showNotification: function (msg, className, width) {
        var settings = {
            closeOnClick: true,
            position: "bottom-left",
            autoOpen: true,
            animationOpenDelay: 500,
            autoClose: true,
            autoCloseDelay: 5000,
            template: null,
            theme: 'uuTheme',
            height: '30px',
            width: width
        };

        $('body').append('<div class="' + className + '">' + msg  + '</div>');
        $('.' + className).last().jqxNotification(settings);
    },

    // Style tweaks
    styleTweak: function () {
        var widget = this;

        $('#' + widget.id)
        // hiding arrow on edit
            .on('cellBeginEdit', toggleEditStyles)
            .on('cellEndEdit', toggleEditStyles)
            .jqxDataTable('hideColumn', 'stateType');

        function toggleEditStyles() {
            $('input').parent().parent().toggleClass('editableUUCell');
        }
    },

    // updating row's Sum and sending new data to the server
    updateRowHandler: function (rowID, rowData, commit, widget) {
        // in a few obscure cases native jqxDataTable validation fails, so we can either rewrite it completely
        // OR just ensure graceful degradation in those situations - I chose ignoring any plainly invalid input
        function parseFloatOrZero(num) {
            return parseFloat(num) || 0
        }

        var homeworkPts = parseFloatOrZero(rowData.HOMEWORK);
        var seminarworkPts = parseFloatOrZero(rowData.SEMINARWORK);
        var testPts = parseFloatOrZero(rowData.TEST);
        var workshopPts = parseFloatOrZero(rowData.WORKSHOP);

        rowData.total = homeworkPts + seminarworkPts + testPts + workshopPts;

        // allowing update to continue, because we don't handle server errors, only notify user
        commit(true);

        var updateData = JSON.stringify({
            "HOMEWORK": homeworkPts,
            "TEST": testPts,
            "SEMINARWORK": seminarworkPts,
            "WORKSHOP": workshopPts,
            "total": rowData.total
        });

        var updateUrl =
            'https://cmd.plus4u.net/ucl-educua/StudentCardCourse/setScore/exec?uuUri=' + rowData.studentCardCourse;

        $.ajax({
                type: 'POST',
                url: updateUrl,
                cache: true,
                contentType: 'application/json',
                data: updateData,
                headers: widget.authCfg
            })
            .done(function () {
                widget.showNotification(widget.localizationConfig.valueSaved, 'server-ok-notification', '90px');
            })
            .fail(function (error) {
                widget.showNotification(widget.localizationConfig.connectionError, 'server-err-notification', '155px');
            });
    }
});

// TODO rewrite stateType Filter (0.75) - currently hacked to work for presentation
//                                  - plan: main control just removes filters and sets config, filters are assigned in
//                                      event catcher, which applies filters again and sets "pass" flag
//                                  - why 2 firings of event? There's no way to easily determine jqxDataTable filters
//                                      from event object & we want to set filters from multiple sources