My Widget - Client Part
=========

This UU widget has one view called *default*

* View functionality (JS) is defined in `views/default.js`
* View structure (HTML) is defined in `html/default.html`
* View design (CSS) is defined in `css/default.html`
* Optional partial HTML or Handlebars templates for view can be added to `templates/default/` directory

Widget can be locally tested by running `uuw try` and opening `127.0.0.1:4567/demo` in browser

Disclaimer
----------
uuw tool which has generated this file is still in alpha version.
For now only one (*default*) view per widget is supported.
