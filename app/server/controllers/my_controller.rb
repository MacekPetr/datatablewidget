require_relative 'uu_widget/uu_widget'

# Require model here...
#require_relative '../model/my_model'

class MyController < WidgetRootController
  set :static, true
  set :public_folder, File.dirname(__FILE__) + '/../../client'

  get '/' do
    "UU Widget MyWidget - Server (#{DateTime.new().to_s})"
  end

  # region View Default
  get '/widget/contentForDefaultView' do
    @uuDTO_out["data"]["body"] = {widget:'MyWidget', time:DateTime.new().to_s}

    #UU::OS::Security::Session.logout() #+4U logout
    @uuDTO_out.to_json
  end

  post 'actionSayHello' do
    @uuDTO_out["data"]["body"] = {message:"Hello, you sent #{@uuDTO_in['data']['body']}"}

    #UU::OS::Security::Session.logout() #+4U logout
    @uuDTO_out.to_json
  end

  # other view actions can be added here...
  # endregion

  # other views can be added here... (not supported yet)
end
