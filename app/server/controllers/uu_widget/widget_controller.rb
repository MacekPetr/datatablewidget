require 'sinatra'
require 'json'
require 'uu_os_client'

class WidgetController < Sinatra::Application
  # Enable error handling in Development mode
  set :show_exceptions, true
  set :raise_errors, true

  # Enable remote calls - Comment for runtime!!!
  set :remote, true

  def buildError
    ((!@uuDTO_out) && (
    @uuDTO_out = {
        "schema" => "UU/OS/DTO",
        "uri" => DateTime.now.to_s,
        "data" => {
          "status" => "",
          "body" => {}
        }
    }))

    @uuDTO_out["data"]["status"]            = status.to_s
    @uuDTO_out["data"]["body"]["message"]   = env['sinatra.error'].message
    @uuDTO_out["data"]["body"]["backtrace"] = env['sinatra.error'].backtrace

    #@uuDTO_out["data"]["body"]["stacktrace"] = env['sinatra.error'].

    ::UU::OS::Security::Session.logout() #+4U logout

    return @uuDTO_out.to_json
  end

  before '/widget/*' do
    cache_control :no_cache, :max_age => 0
    #build uuDTO
    @uuDTO_out = {
        "schema" => "UU/OS/DTO",
        "uri" => DateTime.now.to_s,
        "data" => {
            "status" => "200",
            "body" => {}
        }
    }

    begin
      #login as User, get Personal role, extract uuID, logout
      (!request.env["HTTP_TOKEN"]) && (raise "My_Widget_before_LoginTokenMissing")

      (::UU::OS::Security::Session.login(request.env["HTTP_TOKEN"])) || (raise "My_Widget_before_UserLoginFail")
      @uuID = ::UU::OS::Security::Session.get_personal_role.artifact_code
      ::UU::OS::Security::Session.logout() #+4U logout

      #obtain and check input parameters uuDTO_in (json)
      @inp = request.body.read
      @inp.force_encoding('UTF-8')
      (@inp.encoding.name != 'UTF-8') && (raise "My_Widget_before_InputParametersIsNotUTF-8 ("+@inp.encoding.name+")")
      @inp = "{}" if @inp == ""
      @uuDTO_in = JSON.parse(@inp)

      # test - correct input parameters
      # (
      # (!@uuDTO_in) ||
      #     (!@uuDTO_in.key?("data")) ||
      #     (!@uuDTO_in["data"].key?("body"))
      # ) && (raise "My_Widget_before_WrongInputParameterJSON")

      # test - My Widget Missing
      #(!@uuDTO_in['data']['body'].key?('my_widget')) && (raise "My_Widget_before_WidgetIDMissing")

      # test - uuID missing
      # (!@uuDTO_in['data']['body'].key?('uuID')) && (raise "My_Widget_before_uuIDMissing")

      # login as MyWidget uuEE
      #(::UU::OS::Security::Session.login(File.dirname(__FILE__)+'/access')) || (raise "My_Widget_before_uuEELoginFail")

      # create model
      #@model = MyModel.new @uuDTO_in['data']['body']['my_widget'], @uuID
    end
  end

  # fujky
  error do
    buildError
  end

  # not_found do
  #   buildError
  # end
end
