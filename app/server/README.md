My Widget - Server Part
=========

This UU widget has one (root) controller called *MyController* with one view *default*

Widget can be locally tested by running `uuw try` and opening `127.0.0.1:4567/demo` in browser

Disclaimer
----------
uuw tool which has generated this file is still in alpha version.
For now only one view per controller is supported.
